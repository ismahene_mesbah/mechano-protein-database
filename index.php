<!DOCTYPE html>
<html lang="en">
<head>

     <title>MechanoPro-DB</title>

     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/style.css">

</head>

<?php	
include 'DB_cnx.php'; 


// PIE CHART DATA
$stmt = $db->prepare("SELECT structure, COUNT(structure) as nb  FROM MPDB_Proteins GROUP BY structure"); 
$stmt->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
$structure = array_column($data, 'structure');
$nb = array_column($data, 'nb');



// BOXPLOT DATA
// all alpha proteins
$stmt_forces_alpha= $db->prepare("SELECT unfolding_force  FROM MPDB_Proteins WHERE structure LIKE 'All alpha%'");
$stmt_forces_alpha->execute();
$data_f_alpha = $stmt_forces_alpha->fetchAll(PDO::FETCH_ASSOC);
$f_alpha = array_column($data_f_alpha, 'unfolding_force');
// all beta proteins
$stmt_forces_beta= $db->prepare("SELECT unfolding_force  FROM MPDB_Proteins WHERE structure LIKE 'All beta%'");
$stmt_forces_beta->execute();
$data_f_beta = $stmt_forces_beta->fetchAll(PDO::FETCH_ASSOC);
$f_beta = array_column($data_f_beta, 'unfolding_force');
// alpha + beta
$stmt_forces_a_plus_b= $db->prepare("SELECT unfolding_force  FROM MPDB_Proteins WHERE structure LIKE '%+%'");
$stmt_forces_a_plus_b->execute();
$data_f_a_plus_b = $stmt_forces_a_plus_b->fetchAll(PDO::FETCH_ASSOC);
$f_a_plus_b = array_column($data_f_a_plus_b, 'unfolding_force');
// alpha / beta
$stmt_forces_alphabeta= $db->prepare("SELECT unfolding_force  FROM MPDB_Proteins WHERE structure LIKE '%/%'");
$stmt_forces_alphabeta->execute();
$data_f_alphabeta = $stmt_forces_alphabeta->fetchAll(PDO::FETCH_ASSOC);
$f_alphabeta = array_column($data_f_alphabeta, 'unfolding_force');

// small proteins
$stmt_forces_small= $db->prepare("SELECT unfolding_force  FROM MPDB_Proteins WHERE structure LIKE '%Small%'");
$stmt_forces_small->execute();
$data_f_small = $stmt_forces_small->fetchAll(PDO::FETCH_ASSOC);
$f_small = array_column($data_f_small, 'unfolding_force');

// Min Max forces per class

$stmt_forces_classification= $db->prepare("SELECT MIN(unfolding_force) as min_forces, MAX(unfolding_force) as max_forces, classification FROM MPDB_Proteins GROUP BY classification");
$stmt_forces_classification->execute();
$data_f_classification = $stmt_forces_classification->fetchAll(PDO::FETCH_ASSOC);
$minF = array_column($data_f_classification, 'min_forces');
$maxF = array_column($data_f_classification, 'max_forces');
$classification = array_column($data_f_classification, 'classification');


// force vs vel
$stmt_class= $db->prepare("SELECT DISTINCT classification FROM MPDB_Proteins");
$stmt_class->execute();
$data_class = $stmt_class->fetchAll(PDO::FETCH_ASSOC);
$class = array_column($data_class, 'classification');



//Xu and structure
// BOXPLOT DATA
// all alpha proteins
$stmt_xu_alpha= $db->prepare("SELECT Xu FROM MPDB_Proteins WHERE structure LIKE 'All alpha%'");
$stmt_xu_alpha->execute();
$data_xu_alpha = $stmt_xu_alpha->fetchAll(PDO::FETCH_ASSOC);
$xu_alpha = array_column($data_xu_alpha, 'Xu');
// all beta proteins
$stmt_xu_beta= $db->prepare("SELECT Xu FROM MPDB_Proteins WHERE structure LIKE 'All beta%'");
$stmt_xu_beta->execute();
$data_xu_beta = $stmt_xu_beta->fetchAll(PDO::FETCH_ASSOC);
$xu_beta = array_column($data_xu_beta, 'Xu');
// alpha + beta
$stmt_xu_a_plus_b= $db->prepare("SELECT Xu FROM MPDB_Proteins WHERE structure LIKE '%+%'");
$stmt_xu_a_plus_b->execute();
$data_xu_a_plus_b = $stmt_xu_a_plus_b->fetchAll(PDO::FETCH_ASSOC);
$xu_a_plus_b = array_column($data_xu_a_plus_b, 'Xu');
// alpha / beta
$stmt_xu_alphabeta= $db->prepare("SELECT Xu FROM MPDB_Proteins WHERE structure LIKE '%/%'");
$stmt_xu_alphabeta->execute();
$data_xu_alphabeta = $stmt_xu_alphabeta->fetchAll(PDO::FETCH_ASSOC);
$xu_alphabeta = array_column($data_xu_alphabeta, 'Xu');

// small proteins
$stmt_xu_small= $db->prepare("SELECT Xu  FROM MPDB_Proteins WHERE structure LIKE '%Small%'");
$stmt_xu_small->execute();
$data_xu_small = $stmt_xu_small->fetchAll(PDO::FETCH_ASSOC);
$xu_small = array_column($data_xu_small, 'Xu');





//Koff and structure
// BOXPLOT DATA
// all alpha proteins
$stmt_koff_alpha= $db->prepare("SELECT koff FROM MPDB_Proteins WHERE structure LIKE 'All alpha%'");
$stmt_koff_alpha->execute();
$data_koff_alpha = $stmt_koff_alpha->fetchAll(PDO::FETCH_ASSOC);
$koff_alpha = array_column($data_koff_alpha, 'koff');
// all beta proteins
$stmt_koff_beta= $db->prepare("SELECT koff FROM MPDB_Proteins WHERE structure LIKE 'All beta%'");
$stmt_koff_beta->execute();
$data_koff_beta = $stmt_koff_beta->fetchAll(PDO::FETCH_ASSOC);
$koff_beta = array_column($data_koff_beta, 'koff');
// alpha + beta
$stmt_koff_a_plus_b= $db->prepare("SELECT koff FROM MPDB_Proteins WHERE structure LIKE '%+%'");
$stmt_koff_a_plus_b->execute();
$data_koff_a_plus_b = $stmt_koff_a_plus_b->fetchAll(PDO::FETCH_ASSOC);
$koff_a_plus_b = array_column($data_koff_a_plus_b, 'koff');
// alpha / beta
$stmt_koff_alphabeta= $db->prepare("SELECT koff FROM MPDB_Proteins WHERE structure LIKE '%/%'");
$stmt_koff_alphabeta->execute();
$data_koff_alphabeta = $stmt_koff_alphabeta->fetchAll(PDO::FETCH_ASSOC);
$koff_alphabeta = array_column($data_koff_alphabeta, 'koff');

// small proteins
$stmt_koff_small= $db->prepare("SELECT koff  FROM MPDB_Proteins WHERE structure LIKE '%Small%'");
$stmt_koff_small->execute();
$data_koff_small = $stmt_koff_small->fetchAll(PDO::FETCH_ASSOC);
$koff_small = array_column($data_koff_small, 'koff');






// INTERACTIVE PLOT DATA
$stmt_class_interactive= $db->prepare("SELECT structure FROM MPDB_Proteins");
$stmt_class_interactive->execute();
$data_class_interactive = $stmt_class_interactive->fetchAll(PDO::FETCH_ASSOC);
//$class_interactive = array_column($data_class_interactive, 'structure');

$stmt_force_interactive= $db->prepare("SELECT velocity, unfolding_force, loading_rate FROM MPDB_Proteins");
$stmt_force_interactive->execute();
$data_force_interactive = $stmt_force_interactive->fetchAll(PDO::FETCH_ASSOC);
$force_interactive = array_column($data_force_interactive, 'unfolding_force');
$velocity_interactive = array_column($data_force_interactive, 'velocity');
$lr_interactive = array_column($data_force_interactive, 'loading_rate');



?>
<script> 

</script>


<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">
               <span class="spinner-rotate"></span>
          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">MechanoPro-DB</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                    
                         <li class="active"><a href="index.html">Home</a></li>
                    
                         <li><a href="all_prots.php">Data</a></li> 
                    
                         <li><a href="about-us.html">About Us</a></li>  
                    
                         <li><a href="team.html">Authors</a></li> 
                    
                         <li><a href="InsertInfo.php">Contribute</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <!-- HOME -->
     <section id="home">
          <div class="row">
               <div class="owl-carousel owl-theme home-slider">
                    <div class="item item-first">
                         <div class="caption">
                              <div class="container">
                                   <div class="col-md-6 col-sm-12">
                                        <h1> MechanoProtein DataBase (MechanoPro-DB) </h1>
                                        <h3> MechanoProtein-DB stores information about the mechanical properties of proteins</h3>
                                        <a href="all_prots.php" class="section-btn btn btn-default">Enter the database</a>
                                   </div>
                              </div>
                         </div>
                    </div>




               </div>
          </div>
     </section>

     <main>
          <section>
               <div class="container">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                              <div class="text-center">
                                   <h2>About MechanoPro-DB</h2>

                                   <br>

                                   <p class="lead"> 
												
                                   				MechanoProtein DataBase (MechanoPro-DB) is a freely available database that stores information about the mechanical properties of 
												proteins including
                                   				the unfolding forces, contours lengths, energy landscape parameters. 
												More importantly, a manual annotation of the unfolding process and pathway are provided.
                              	</p>
                              </div>
                         
                       
      					<div  id='myDivvel'  >         

  						<script src='https://cdn.plot.ly/plotly-2.18.0.min.js'></script>                 
  
   						<script>   


   
						<?php 

							$i=0;
                        /*
                        	const colors=     ['#ffca28' , 
                                          '#000033',
                                          '#FF6666',
                                          '#FFCC99',
                                           '#DAF7A6',
                                           '#FFC300',
                                           '#bf033b',
                                           '#b0a696',
                                                  '#000033',
                                          '#FF6666',
                                          '#FFCC99',
                                           '#DAF7A6',
                                           '#FFC300',
                                           '#bf033b',
                                           '#b0a696',   '#000033',
                                          '#FF6666',
                                          '#FFCC99',
                                           '#DAF7A6',
                                           '#FFC300',
                                           '#bf033b',
                                           '#b0a696',
                                                  '#000033',
                                          '#FF6666',
                                          '#FFCC99',
                                           '#DAF7A6',
                                           '#FFC300',
                                           '#bf033b',
                                           '#b0a696'
               							 ];
                        */
                        
							foreach ($class as $c) {
								$stmt_f_vel= $db->prepare("SELECT velocity, unfolding_force FROM ExperimentalProteins WHERE classification LIKE '%". $c . "%' ;");
								$stmt_f_vel->execute();
								$data_f_vel = $stmt_f_vel->fetchAll(PDO::FETCH_ASSOC);
								$f = array_column($data_f_vel, 'unfolding_force');

								$vel = array_column($data_f_vel, 'velocity');
                            
                            	//print_r($c);

								//remove null value from array of velocity and array of force
								$vel = array_map(function($v){  return (is_null($v)) ? "" : $v;},$vel);

								$f = array_map(function($v){    return (is_null($v)) ? "" : $v;},$f);

								print "<script> console.log(".  json_encode($f)." ) </script>";
								print "<script> console.log(".  json_encode($vel)." ) </script>";
								print "<script> const data_vel_f= []; console.log(".  json_encode($c)." ) ;
                                
                                
						const colors=[	'#a32e58',
										'#0000FF',
										'#18263d',
										'#008080',
										'#7b3030',
										'#f4db9a',
										'#cd2200',
										'#ecb939',
										'#85b6c1',
										'#b3e2f1',
										'#194747',
										'#ed84b2',
										'#00FF00',
										'#d406e3',
										'#646e8f',
										'#6f576f',
										'#00FFFF',
										'#e0d9cd',
										'#4000ff',
										'#704684',
										'#b6a3bf',
										'#bec7b9',
										'#83917a',
										'#d2dddd',
										'#a2bcbd',
										'#799a80',
										'#9784a4',
										'#c26f60'
               						]         
                                
                                
                                
                                </script>";

								print "<script>   

								var trace". json_encode($i)." = {
  									x: ". json_encode($vel). " ,
  									y: ".json_encode($f)." ,
  									mode: 'markers',
  									type: 'scatter',
  									name:  " .json_encode($c) ." ,
  									marker: { 
                                    			size: 12,
                                                color: colors[" . json_encode($i)."]

                                                
                                                },
  									hovertemplate: '<i>Force</i>: %{y}' +'<br><b>Velocity</b>: %{x}<br>'
									};

								console.log(trace". json_encode($i)." ) ;

								data_vel_f.push(trace". json_encode($i).");
								console.log(data_vel_f ) ;

									</script>";


								$i ++ ;

							}; 
     
 
 
						print "<script>   

						var layout = {
     						xaxis: {
      							type: 'log',

     							title: 'Velocity [nm/s]'

     							},
     						yaxis: {
            					type: 'log',

     							title: 'Force [pN]'
     							},    
 
  							title:'Force versus velocity'
							};
                                                                 
						var config = {

  							showEditInChartStudio: true,
  							plotlyServerURL: 'https://chart-studio.plotly.com'
							};

						Plotly.newPlot('myDivvel', data_vel_f, layout, config);
        				</script>    ";                            
 
      
      
       			  ?>

 			</script>
 </div>
                  
                    <!-- Xu  with respect to protein fold-->
                  <div id='divXu'>
                  						 	<div ><!-- Plotly chart will be drawn inside this DIV --></div>
										<script>

											var all_alpha=  <?php echo json_encode($xu_alpha); ?>;
                                            var all_beta=  <?php echo json_encode($xu_beta); ?>;
                                            var alpha_beta_plus=  <?php echo json_encode($xu_a_plus_b); ?>;
                                            var alpha_beta_alt=  <?php echo json_encode($xu_alphabeta); ?>;
                                            var small=  <?php echo json_encode($xu_small); ?>;

											//var all_beta= [127,  204, 230, 74 ,124 , 89 	,138 	, 210 ,15 	, 177 , 40 	,60 ,480 	, 425 ,214 , 1600, 1700 , 146 	,80, 530, 385, 356, 272, 429, 473, 363 ];
											//var alpha_beta_plus= [37, 450, 64, 70 , 85, 203, 230, 104, 104, 117, 350, 407, 346, 356, 548, 152, 190, 47, 73, 127, 117,  117, 115];
											//var alpha_beta_alt= [19]


											var trace1 = {
 												 y: all_alpha,
  												 name: 'all alpha proteins',
  												 type: 'box'

												};

										   var trace2 = {
  												y: all_beta,
  												name: 'all beta proteins',
  												type: 'box'

												};

										   var trace3 = {
  												y: alpha_beta_plus,
  												name: 'alpha beta (a+b)',
  												type: 'box'
												};
										  var trace4 = {
  												y: alpha_beta_alt,
  												name: 'alpha beta (a/b)',
  												type: 'box'
												};

										  var trace5 = {
  												y: small,
  												name: 'Small Proteins',
  												type: 'box'
												};

										  var layout = {
  												title: 'Distance to the transition state (nm) versus Protein Fold',
                                          		yaxis: {
                                                type: 'log',
    											title: 'X𝛃 [nm]'
                                                },
                                            	colorway : ['green', 'darkblue', 'orange', 'red', 'purple']
    												};
                                        
                                         var config = {

  											showEditInChartStudio: true,

  											plotlyServerURL: "https://chart-studio.plotly.com"

												};

    
										  var data = [trace1, trace2, trace3, trace4, trace5];
												Plotly.newPlot('divXu', data, layout, config);	
  										</script>  
                  
                  </div>
                  
                                    
                    <!--koff with respect to protein fold-->
                  <div id='divkoff'>
                  						 	<div ><!-- Plotly chart will be drawn inside this DIV --></div>
										<script>

											var all_alpha=  <?php echo json_encode($koff_alpha); ?>;
                                            var all_beta=  <?php echo json_encode($koff_beta); ?>;
                                            var alpha_beta_plus=  <?php echo json_encode($koff_a_plus_b); ?>;
                                            var alpha_beta_alt=  <?php echo json_encode($koff_alphabeta); ?>;
                                            var small=  <?php echo json_encode($koff_small); ?>;

											//var all_beta= [127,  204, 230, 74 ,124 , 89 	,138 	, 210 ,15 	, 177 , 40 	,60 ,480 	, 425 ,214 , 1600, 1700 , 146 	,80, 530, 385, 356, 272, 429, 473, 363 ];
											//var alpha_beta_plus= [37, 450, 64, 70 , 85, 203, 230, 104, 104, 117, 350, 407, 346, 356, 548, 152, 190, 47, 73, 127, 117,  117, 115];
											//var alpha_beta_alt= [19]


											var trace1 = {
 												 y: all_alpha,
  												 name: 'all alpha proteins',
  												 type: 'box'

												};

										   var trace2 = {
  												y: all_beta,
  												name: 'all beta proteins',
  												type: 'box'

												};

										   var trace3 = {
  												y: alpha_beta_plus,
  												name: 'alpha beta (a+b)',
  												type: 'box'
												};
										  var trace4 = {
  												y: alpha_beta_alt,
  												name: 'alpha beta (a/b)',
  												type: 'box'
												};

										  var trace5 = {
  												y: small,
  												name: 'Small Proteins',
  												type: 'box'
												};

										  var layout = {
  												title: 'Unfolding rate koff (s⁻¹) versus Protein Fold',
                                          		yaxis: {
                                                type: 'log',
    											title: 'Unfolding rate (koff) [s⁻¹]'
                                                },
                                            	colorway : ['green', 'darkblue', 'orange', 'red', 'purple']
    												};
                                        
                                         var config = {

  											showEditInChartStudio: true,

  											plotlyServerURL: "https://chart-studio.plotly.com"

												};

    
										  var data = [trace1, trace2, trace3, trace4, trace5];
												Plotly.newPlot('divkoff', data, layout, config);	
  										</script>  
                  
                  </div>
                  
                  
                         </div>
                    </div>
               </div>
          </section>
          <section>
               <div class="container">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                              <div class="section-title text-center">
                                   <h2>Latest Entries  <small> As of Tue Dec 14 2022</small></h2>
                              </div>
                         </div>
  <!-- stat1 -->

                    
                    <div class="col-md-4 col-sm-4">
                              <div class="courses-thumb courses-thumb-secondary">
                                 <div class="courses-top" id='StatDiv'>
                                           <script src='https://cdn.plot.ly/plotly-2.16.1.min.js'></script>          
       										<script>
       											var structure = <?php echo json_encode($structure); ?>; // recuperer la variable structure et la transformer en js
       											var nb = <?php echo json_encode($nb); ?>;

                                                
												var data = [{
  													values: nb,
                                                  	labels:  structure ,

  												//	labels: ["All alpha proteins", "All beta proteins", "Alpha and beta proteins (a+b)",  "Alpha and beta proteins (a/b)" ],
  													hoverinfo: 'label+percent',
  													hole: .4,
  													type: 'pie'
															}];

												var layout = {
  													title: 'Statistics of available proteins structures',


                                                showlegend: true

														};
                                            
                                                 var config = {

  													showEditInChartStudio: true,

  													plotlyServerURL: "https://chart-studio.plotly.com"

														};
                                            
													Plotly.newPlot('StatDiv', data, layout, config);
                                 				</script>

                                   </div>

                                   <div class="courses-detail">
                                    <!--  Title of Paper   !-->

                                        <h3><a href="#">Protein structures are classified according to SCOP annotations</a></h3>
                                   </div>

                                   <div class="courses-info">
                                        <a href="#" class="section-btn btn btn-primary btn-block">More Details</a>
                                   </div>
                              </div>
                         </div>
<!-- stat2 -->
                         <div class="col-md-4 col-sm-4">
                              <div class="courses-thumb courses-thumb-secondary">
                                   <div class="courses-top" id='myDiv'>

							 	<div ><!-- Plotly chart will be drawn inside this DIV --></div>
										<script>

											var all_alpha=  <?php echo json_encode($f_alpha); ?>;
                                            var all_beta=  <?php echo json_encode($f_beta); ?>;
                                            var alpha_beta_plus=  <?php echo json_encode($f_a_plus_b); ?>;
                                            var alpha_beta_alt=  <?php echo json_encode($f_alphabeta); ?>;
                                            var small=  <?php echo json_encode($f_small); ?>;

											//var all_beta= [127,  204, 230, 74 ,124 , 89 	,138 	, 210 ,15 	, 177 , 40 	,60 ,480 	, 425 ,214 , 1600, 1700 , 146 	,80, 530, 385, 356, 272, 429, 473, 363 ];
											//var alpha_beta_plus= [37, 450, 64, 70 , 85, 203, 230, 104, 104, 117, 350, 407, 346, 356, 548, 152, 190, 47, 73, 127, 117,  117, 115];
											//var alpha_beta_alt= [19]


											var trace1 = {
 												 y: all_alpha,
  												 name: 'all alpha proteins',
  												 type: 'box'

												};

										   var trace2 = {
  												y: all_beta,
  												name: 'all beta proteins',
  												type: 'box'

												};

										   var trace3 = {
  												y: alpha_beta_plus,
  												name: 'alpha beta (a+b)',
  												type: 'box'
												};
										  var trace4 = {
  												y: alpha_beta_alt,
  												name: 'alpha beta (a/b)',
  												type: 'box'
												};

										  var trace5 = {
  												y: small,
  												name: 'Small Proteins',
  												type: 'box'
												};

										  var layout = {
  												title: 'Forces versus Protein Fold',
                                          		yaxis: {
                                                type: 'log',
    											title: 'Force [pN]'
                                                },
                                            	colorway : ['green', 'darkblue', 'orange', 'red', 'purple']
    												};
                                        
                                         var config = {

  											showEditInChartStudio: true,

  											plotlyServerURL: "https://chart-studio.plotly.com"

												};

    
										  var data = [trace1, trace2, trace3, trace4, trace5];
												Plotly.newPlot('myDiv', data, layout, config);	
  										</script>                                 
                                   </div>

                                   <div class="courses-detail">
                                   <!--  Title of Paper   !-->
                                        <h3><a href="#"> Proteins composed of beta sheets have been shown to unfold at higher forces than those composed of alpha helixes</a></h3>
                                   </div>

                                   <div class="courses-info">
                                        <a href="#" class="section-btn btn btn-primary btn-block">More Details</a>
                                   </div>
                              </div>
                         </div>
<!-- stat3 -->
                         <div class="col-md-4 col-sm-4">
                              <div  class="courses-thumb courses-thumb-secondary">
                              <div id="DivClassForce" > </div>
                              
      							<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
                                
  									<script>
                                    
                                    var classification=  <?php echo json_encode($classification); ?>;

                                  //  var classification = [ 'STRUCTURAL PROTEIN',  'IMMUNOGLOBULIN-LIKE DOMAIN',  'CELL ADHESION PROTEIN' , 'HEPARIN AND INTEGRIN BINDING', 'MUSCLE PROTEIN',
									//	'HYDROLASE(ENDORIBONUCLEASE)',  'ACETYLTRANSFERASE' , 'STRUCTURAL GENOMICS', 'UNKNOWN FUNCTION' , 'ENDOCYTOSIS/EXOCYTOSIS',
								//		'HYDROLASE' , 'MICROBIAL RIBONUCLEASE', 'CALCIUM-BINDING PROTEIN', 'CHROMOSOMAL PROTEIN', 'LUMINESCENT PROTEIN' , 'FLUORESCENT PROTEIN', 'PROTEIN BINDING',
								//		'IMMUNOGLOBULIN BINDING PROTEIN', 'COHESIN', 'TRANSFERASE',  'PHOTORECEPTOR', 'CYTOSKELETON', 'TRIPLE-HELIX COILED COIL', 'IMMUNE SYSTEM'];

                                    
								//	var minForce = [ 27, 204, 74, 89, 210, 19, 15, 40, 40, 60, 64, 70, 20, 85, 104, 117,  152, 190, 214, 47, 150, 30, 38, 80 ];
                                    var minForce=  <?php echo json_encode($minF); ?>;

                                    

								//	var maxForce = [480, 530, 138, 124, 210, 19, 177, 40, 40, 60, 64, 70, 20, 230, 104, 548, 152, 190, 214, 73, 150, 70, 38, 146];
                                    var maxForce=  <?php echo json_encode($maxF); ?>;

									var trace1 = {
  										type: 'scatter',
  										x: minForce,
 	 									y: classification,
  										mode: 'markers',
                                        name: 'Min forces',
  										marker: {

    											symbol: 'circle',
  												}
											};

									var trace2 = {
  										x: maxForce,
  										y: classification,
                                        name: 'Max forces',
  										mode: 'markers',
  										marker: {

    									symbol: 'circle',
  											}
											};

								var data = [trace1, trace2];

								var layout = {
  									title: 'Min and Max Forces per Functional Category',
 									 xaxis: {
    									showgrid: false,
                                        tickfont: {
      												size: 9,
      												color: 'black'
    												},
    									showline: true,
                                        automargin: true,
                                     	type: 'log',
                                     	title: 'Force [pN]'

                                     },
                                  yaxis: {
									//range: [0 , 25],
                                 //  	showticklabels: true,
    								automargin: true,
    								tickfont: {
      										size: 7,
      										color: 'black'
    										}
                                  },
                                  showlegend: false
                          
								};
                                var config = {

  									showEditInChartStudio: true,

  									plotlyServerURL: "https://chart-studio.plotly.com"

									};

								Plotly.newPlot('DivClassForce', data, layout, config, {scrollZoom: true});

                              		</script>
                              

                                   <div class="courses-detail">
                                   <!--  Title of Paper   !-->
                                        <h3><a href="#"> Force range of all available proteins clustered according to their functional category</a></h3>
                                   </div>

                                   <div class="courses-info">
                                        <a href="#" class="section-btn btn btn-primary btn-block">More Details</a>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </section>
     </main>

                                    
<!-- INTERACTIVE PLOTS -->
                                    
	<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>

  		<div>
    		<style>
    			#plot-container {
      				display: flex;
      				justify-content: center;
      				align-items: center;
      				height: 100vh;
    								}
  			</style>
  		</div>
	<script>

	</script>

  		<div>
    		<label for="x-data">X Data:</label>
    			<select id="x-data">
      				<option value= <?php echo json_encode($force_interactive);?>.  > Force (pN)</option>
      				<option value= <?php echo json_encode($velocity_interactive);?>. > Velocity (nm/s)</option>
      				<option value= <?php echo json_encode($lr_interactive);?>.> Loading Rate (pN/s)</option>
    			</select>
  		</div>
  		<div>
    		<label for="y-data">Y Data:</label>
    			<select id="y-data">
      				<option value = <?php echo json_encode($force_interactive);?>.  > Force (pN)</option>
      				<option value=<?php echo json_encode($velocity_interactive);?>. > Velocity (nm/s)</option>
      				<option value=<?php echo json_encode($lr_interactive);?>.> Loading Rate (pN/s) </option>
    			</select>
  		</div>
  		<div>
    		<label for="log-scale">Log Scale Y:</label>
    		<input type="checkbox" id="log-scale" />
  		</div>


  		<div>
    		<label for="log-scale_X">Log Scale X:</label>
    		<input type="checkbox" id="log-scale_X" />
  		</div>


  		<div>
    		<label for="plot-type">Plot Type:</label>
    			<select id="plot-type">
      				<option value="scatter">Scatter</option>
      				<option value="line">Line</option>
      				<option value="bar">Bar</option>
      				<option value="pie">Pie</option>
      				<option value="box">Box</option>
    			</select>
  		</div>
  
        <div>
    		<button onclick="plotData()">Plot</button>
  		</div>
  		<div id="plot"></div>

  		<script>
    	function plotData() {
      			var xDataSelect = document.getElementById('x-data');
      			var yDataSelect = document.getElementById('y-data');
      			var plotType = document.getElementById('plot-type').value;
      			var logScale = document.getElementById('log-scale').checked;
      			var logScale_X = document.getElementById('log-scale_X').checked;

      			// Get the selected values from the dropdowns
      			var xData1 = Array.from(xDataSelect.selectedOptions, option => option.value);
    			var xDataString = xData1[0].replace(/"/g, ""); // Remove double quotes from the string
				var xDataValues = xDataString.split(",");
				var xData = xDataValues.map(value => Number(value));
    


     			// var xData = JSON.parse(JSON.parse(xDataString));

    			//  var db_x = JSON.stringify(xDataString);
     			// var xData = JSON.parse(xDataString);  // Parse the string into an array



      			var yData1 = Array.from(yDataSelect.selectedOptions, option => option.value);
   				//   var yDataString = yData1[0].trim();  // Extract the string from the array
     			// var yDataValues = yDataString.slice(1, -1).split(",");
     			// var yData = yDataValues.map(value => Number(value));

    			var yDataString = yData1[0].replace(/"/g, ""); // Remove double quotes from the string
    			var yDataValues = yDataString.split(",");
    			var yData = yDataValues.map(value => Number(value));

      			// Create a trace object based on the selected plot type
      			var trace;
      				if (plotType === 'scatter' || plotType === 'line') {
        				trace = {
          						x: xData,
          						y: yData,
        						mode: 'markers',
          						type: plotType
        					};
      			}
            	    else if (plotType === 'bar') {
        				trace = {
          						x: xData,
          						y: yData,
          						type: 'bar'
        					};
      			} else if (plotType === 'pie') {
        				trace = {
          					labels: xData,
          					values: yData,
          					type: 'pie'
        				};
      			} else if (plotType === 'box') {
        			trace = {
          				y: yData,
          				type: 'box'
        				};
      				}

      			// Create the data array
      		var data = [trace];

      			// Create the layout object
      		var layout = {
        		title: 'Interactive Plot',
        	//	xaxis: { title: 'X-axis' },
                xaxis: { title: 'X-axis', type: logScale_X ? 'log' : 'linear' },

        	//	yaxis: { title: 'Y-axis' },
              	yaxis: { title: 'Y-axis', type: logScale ? 'log' : 'linear' }

      		};
      		var config = {

           		showEditInChartStudio: true,

            	plotlyServerURL: "https://chart-studio.plotly.com"

           		};

      		// Create the plot
      		Plotly.newPlot('plot', data, layout, config);
    	}
  		</script>




     <!-- CONTACT -->
     <section id="contact">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                              <div class="section-title">
                                   <h2>Contact us <small>we love scientific conversations. let us talk!</small></h2>
                              </div>

                             <form id="message" action="message_from_index.php" method="POST" >  

                              <div class="col-md-12 col-sm-12">
                                   <input type="text" class="form-control" placeholder="Enter full name" name="name" required>
                    
                                   <input type="email" class="form-control" placeholder="Enter email address" name="email" required>

                                   <textarea class="form-control" rows="6" placeholder="Tell us about your message" name="full_message"   required></textarea>
                              </div>

                              <div class="col-md-4 col-sm-12">

                                  <input type="submit" class="form-control" name="send message" value="Send Message" form= "message" >
                              </div>
                             </form>

                    </div>

                    <div class="col-md-6 col-sm-12">
                         <div class="contact-image" style="display: grid; grid-template-columns: 1fr 1fr; gap: 60px;justify-items: center; align-items: center;">
                         <img src="images/lai-logo.png" class="img-responsive"   style="padding:0px 10px; border-right: 1px  #000000;">
                         <img src="images/logo-CENTURI.png" class="img-responsive"height="6" style="padding: 0px 10px; border-right: 0.5px  #000000;">
                         <img src="images/Logo_AMU.png" class="img-responsive"  style="padding: 0px 2px; border-left: 1px  #000000;">
                          <img src="images/logo-ibdm.png" class="img-responsive" style="padding-left: 1px  #000000;">
                          <img src="images/Inserm-logo.png" class="img-responsive"  style="padding-right: 0px 10px; ">


                         
                         </div>
                    </div>

               </div>
          </div>
     </section>       

     <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Address</h2>
                              </div>
                              <address>
                                   <p>163 boulevard de Luminy<br> Marseille 13009 </p>
                              </address>

  

                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 Aix Marseille Université</p>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                           
                            

                              <div class="footer_menu">
                                   <h2>Quick Links</h2>
                                   <ul>
                                        <li><a href="index.php">Home</a></li>
                                        <li><a href="about-us.html">About Us</a></li>
                                        <li><a href="terms.html">Terms & Conditions</a></li>
                                        <li><a href="InsertInfo.php">Contact Us</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Newsletter Signup</h2>
                              </div>
                              <div>
                                   <div class="form-group">

                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required>
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

     <!-- SCRIPTS -->
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>

</body>
</html>
