<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["the_file"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["Upload"])) {
  $check = getimagesize($_FILES["the_file"]["tmp_name"]);
  if($check !== false) {
    echo "File is OK - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    echo "File is not OK.";
    $uploadOk = 0;
  }
}

// Check if file already exists
if (file_exists($target_file)) {
  echo "Sorry, file already exists.";
  $uploadOk = 0;
}

// Check file size
if ($_FILES["the_file"]["size"] > 500000) {
  echo "Sorry, your file is too large.";
  $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "csv" && $imageFileType != "txt" && $imageFileType != "dat"
&& $imageFileType != "xls" ) {
  echo "Sorry, only CSV , DAT , TXT & XLS files are allowed.";
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["the_file"]["tmp_name"], $target_file)) {
   print " <body>

			<div class='thank-you-container'>
			<div class='thank-you-box'>
  			<p class='lead'>'The file ". htmlspecialchars( basename( $_FILES['the_file']['name'])). " has been uploaded.' </p>
			</div>
			</div>
		</body>
       ";
  
    //echo "The file ". htmlspecialchars( basename( $_FILES["the_file"]["name"])). " has been uploaded.";
  } else {
    echo "Sorry, there was an error uploading your file.";
  }
}
?>
