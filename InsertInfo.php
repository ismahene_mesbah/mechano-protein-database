<!DOCTYPE html>
<html>
<head>
<title> [Insert] MechnoPro-DB</title>

     <meta charset="UTF-8">
   
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">


     <!-- SCRIPTS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>






     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/data_style.css">

</head>

<body id="top">

     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" >
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">MP-DB</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="index.php">Home</a></li>
                         <li ><a href="all_prots.php">Data</a></li>
                         <li><a href="about-us.html">About Us</a></li>
                         <li><a href="team.html">Authors</a></li>
                         <li class="active"><a  href="InsertInfo.php" >Contribute</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <!--<section style="background-image: url(images/background.png); backgrounds-repeat: no-repeat;"> -->
     
     <section>
          <div class="container">
               <div class="text-center">
                    <h1>MechanoProtein DataBase (MP-DB)</h1>

                    <br>

                    <p class="lead"> Add a Protein to MP-DB</p>
               </div>
          </div>
     </section>

     <section class="section-background"> 


		<div class="container">
  				<form id="fill" action="action_page.php" method="POST" >  </form>


  
      
    				<label for="pb">I have the PDB ID</label>
					<input type="checkbox" name="pb" id="pb" form="fill">
					<br>
  					<br>

   					 <label for="pdb_id" >PDB ID</label>
   					 <input id="pdb_id" name="pdb_id" placeholder="PDB ID as it is referred to in the Protein Data Bank.." form="fill">
        
    				<label for="common_name" class="labelHiddenOrVisible">Common Name</label>
    				<textarea form="fill" type="text" id="common_name" name="common_name"  class="labelHiddenOrVisible" placeholder="Has to be short and general,  example: spectrin, fibronectin. THIS PARAMETER IS MANDATORY IF THE PROTEIN DOES NOT HAVE AN AVAILABLE STRUCTURE IN PDB. This parameters is necessary to avoid having duplicates." ></textarea>
    

  
    				<label for="name" class="labelHiddenOrVisible">Name</label>
   					 <textarea form="fill" id="name" name="name"  class="labelHiddenOrVisible" placeholder="Make sure you give the complete name. eg., SINGLE COHESIN DOMAIN FROM THE SCAFFOLDING PROTEIN CIPA OF THE CLOSTRIDIUM THERMOCELLUM CELLULOSOME.."></textarea>
  

    				<label for="organism" class="labelHiddenOrVisible"> Organism </label>
   					 <input form="fill"  type="text"  id="organism" name="organism" >
  
    
    				<label for="mutation" class="labelHiddenOrVisible"> Mutations (Yes/No)</label>
					<select name="forMutation" class="labelHiddenOrVisible" form="fill">
  						<option value="">Select...</option>
  						<option value="yes">Yes</option>
  						<option value="no">No</option>
					</select>


   					<label for="structure" class="labelHiddenOrVisible">Structure (according to SCOP annotation)</label>
					<select name="FormStructure" class="labelHiddenOrVisible" form="fill">
  						<option value="">Select...</option>
  						<option value="alpha">All Alpha Proteins</option>
  						<option value="beta">All Beta Proteins</option>
      					<option value="a+b">Alpha Beta Proteins (a+b)</option>
      					<option value="a/b">Alpha Beta Proteins (a/b)</option>
					</select>
  
     
  
        
    				<label for="classification" class="labelHiddenOrVisible"> Classification </label>
    				<textarea form="fill" type="text" id="classification" name="classification"  class="labelHiddenOrVisible" placeholder="You can refer to Uniprot (example: muscle protein, binding protein,; cell adhesion protein, chromosomal protein ..etc)"></textarea>
    
    				<label for="doi_pulling" > DOI Pulling </label>
    				<input form="fill" type="url"  id="doi_pulling" name="doi_pulling"  placeholder="example: https://doi.org/10.1016/j.jmb.2003.09.036" required>
    
  
<div class="parms">
    
    		<div>
    			<label for="force" > Unfolding/Ubinding Force [pN] </label>
    			<input  form="fill"  type="number"  step="0.001" id="force" name="force" required>
    			<div id="dynamic_field_force">
    			</div>
			</div>

    

  		<div>
    		<label for="velocity"  > Velocity [nm/s]</label>
    		<input form="fill"  type="number"  step="0.001"    id="velocity" name="velocity"  >
    		<div id="dynamic_field_velocity">
    		</div>
    	</div>
  
    	<div>
    		<label for="loading_rate" >Loading Rate [pN/s]</label>
    		<input form="fill"  type="number"  step="0.00001" id="loading_rate" name="loading_rate" >
    		<div id="dynamic_field_loading">
    		</div>
  		</div>
  
    	<div style="display:flex; flex-direction:column;">
			<button form="fill" class="button" type="button" id="add">Add </button>
			<button form="fill" type="button" class="btn_remove hidden">Remove last</button>
    	</div>
</div>

  


 	<div> Or upload a file with force values and their corresponding loading rates and/or velocities:     </div>
	<br>
	<div>
  <a href='Template_MPDB.xlsx' target='__blank'>   <p> Click here to download a template (fill it & upload it)</p> </a>
    </div>


    
  <br>
  <input type="file" name="the_file" id="the_file" form="download">
  <br>
  <input type="button" value="Upload" form= "download" onclick="redirect()">
  <form id="download" action="upload_file.php" method="post" enctype="multipart/form-data"  >
  <iframe  id="myiframe"  name="hidden-iframe"  style="display:block; visibility:hidden" height="40px" width=" 100%"></iframe>
  </form>

        <label for="pathway"   >Enter suggested unfolding pathway: </label>
        <textarea form="fill" type="text" id="pathway" name="pathway"   placeholder="Respect the following format: N_Ii[Lci]_I[Lci+1]_I[Lci+2]_U[Lcf] where Lcf is the final contour length (maximal physical extension) "></textarea>
        

  <br>
   <div  tyle="display:flex; flex-direction:column;">
    <label for="mail" >Email</label>
    <input  form="fill" id="mail" name="mail" required>
  
      <label for="comment" >Comment</label>
    <textarea  form="fill" id="comment" name="comment" ></textarea>
  </div>
  
    <input form="fill" type="submit" value="Submit" id="submit">

</div>

  
 </section>

</body>


<script>
	function redirect()
	{
		//'my_iframe' is the name of the iframe
		document.getElementById('download').target = 'hidden-iframe';
		document.getElementById('download').submit();
		var iframe = document.getElementById("myiframe");  
		iframe.style.visibility="visible";

	}

$(document).ready(function(){

  $("#on").click(function(){
    if ( $("myiframe").css('visibility') = 'hidden') {
      $("myiframe").css('visibility') = 'visible';
    }
    else {
      $("myiframe").css('visibility') = 'hidden';
    }
  });

});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>




<script>
//ADD button that adds  fields: force, velocity, and loading rate
var k = 1;
$(document).ready(function() {
  $('#add').click(function() {
    if (k <= 3) {
      $('#dynamic_field_loading').append('<div id="row_loading' + k + '"><label" for="loading_rate_' + k + '"></label><input   type="number"  step="0.000001" class="added_fields" name="loading_rate_' + k + '" id="loading_rate_' + k + '" value=""  required></div>')
      $('#dynamic_field_velocity').append('<div id="row_velocity' + k + '"><label" for="velocity_' + k + '"></label><input   type="number"  step="0.000001" class="added_fields" name="velocity_' + k + '" id="velocity_' + k + '" value=""  required></div>')
      $('#dynamic_field_force').append('<div id="row_force' + k + '"><label" for="force_' + k + '"></label><input   type="number"  step="0.000001" class="added_fields" name="force_' + k + '" id="force_' + k + '" value=""  required></div>')

    	k++;
      $('.btn_remove').removeClass('hidden');

    }
  });
  $(document).on('click', '.btn_remove', function() {
    var button_id = $(this).attr("id");
    k--;
    $('#row_loading' + $('#dynamic_field_loading div').length).remove();
    $('#row_force' + $('#dynamic_field_force div').length).remove();
    $('#row_velocity' + $('#dynamic_field_velocity div').length).remove();

    if (k<=1) {
      $('.btn_remove_loading').addClass('hidden');
      $('.btn_remove_force').addClass('hidden');
      $('.btn_remove_velocity').addClass('hidden');

    }
  });
});

/*
//TO make either loading rate or velocity required
jQuery(function ($) {
    var $inputs = $('input[name=velocity],input[name=loading_rate]');
    $inputs.on('input', function () {
        // Set the required property of the other input to false if this input is not empty.
        $inputs.not(this).prop('required', !$(this).val().length);
    });
});

jQuery(function ($) {
    var $inputs = $('input[name=velocity_1],input[name=loading_rate_1]');
    $inputs.on('input', function () {
        // Set the required property of the other input to false if this input is not empty.
        $inputs.not(this).prop('required', !$(this).val().length);
    });
});

*/

</script>
   <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Address</h2>
                              </div>
                              <address>
                                   <p> 163 boulevard de Luminy <br> 13009 Marseille</p>
                              </address>


                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 Aix Marseille Université </p>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              

                              <div class="footer_menu">
                                   <h2>Quick Links</h2>
                                   <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about-us.html">About Us</a></li>
                                        <li><a href="terms.html">Terms & Conditions</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>
  

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Newsletter Signup</h2>
                              </div>
                              <div>
                                   <div class="form-group">
                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required>
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

    


<style>

body {font-family: 'Muli', sans-serif;}
* {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  width: 20%;
  font-size: 15px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  display: flex;
  justify-content: center;
 /* margin:auto;*/
}

input[type=submit] {
  background-color: #45a049;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}



input[type="text"] {
    display: block;
}

input[type="checkbox"]:checked ~ input[type="text"]{
    display: none;
}

.labelHiddenOrVisible{
    display: block;

}

input[type="checkbox"]:checked ~ .labelHiddenOrVisible{
    display: none;

}



#pdb_id
{
 width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}
.container-fluid
{
  display: flex;
  justify-content: center;
  margin:auto;

}

#velocity
{
  display: block;
  width: 60%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

#force
{
  display: block;
  width: 60%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}
#doi_pulling
{
  display: block;
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}
#loading_rate
{
  display: block;
  width: 60%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}
#mail
{
  display: block;
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}
.added_fields
{
  display: block;
  width: 60%;

  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}


.button
{
  display: block;
  width: 25%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
  background-color: greenyellow;

}


.btn_remove
{
  display: block;
  width: 25%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
  background-color: red;

}


.hidden 
{
  display: none;
}

.parms 
{
	display: grid;
	grid-template-columns: 1fr 1fr 1fr;
  	align-items: center;
  	justify-content: center;
    vertical-align:middle;

}

.title
{
	font-weight: 600;
	font-family: 'Titillium Web', sans-serif;
	position: center;  
	font-size: 36px;
	line-height: 200px;
	padding: 15px 15px 15px 15%;
	color:  black;
	box-shadow: 
		inset 0 0 0 1px rgba(22, 160, 133, 1), 
		inset 0 0 5px rgba(53,86,129, 0.4),
		inset -285px 0 35px white;
	border-radius: 0 10px 0 10px;
}
.titre {
	font-weight: normal;
	position: center;
	text-shadow: 0 -1px rgba(0,0,0,0.6);
	font-size: 28px;
	line-height: 40px;
	background:  #04AA6D;
	border: 1px solid #fff;
	padding: 5px 15px;
	color: white;
	border-radius: 0 10px 0 10px;
	box-shadow: inset 0 0 5px rgba(53,86,129, 0.5);
	font-family: 'Muli', sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: center;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: white;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}
</style>



</html>