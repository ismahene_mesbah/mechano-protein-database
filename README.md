# MechanoProtein DataBase
MechanoProtein DataBase (MPDB) is a free database that stores experimental and Moecular Dynamics (MD) simulation data of proteins unfolding. In MPDB, we store relevent information such as the unfolding forces, the loading rates, velocities but most importantly we provide an annotation of the unfolding pathway.

## Getting started
To use MPDB you can either use the web server:
[https://ismahene-mesbah-etu.pedaweb.univ-amu.fr/extranet/MechanoProtein/index.php](https://ismahene-mesbah-etu.pedaweb.univ-amu.fr/extranet/MechanoProtein/index.php)

Or download the current GitLab repository via the command below:

`git clone https://gitlab.com/ismahene_mesbah/mechano-protein-database.git`

All data is provided in the `AllProteins.sql` and `AllProteins.csv`  files.

# Tutorial

The provided link above takes you to the [Main Page](https://ismahene-mesbah-etu.pedaweb.univ-amu.fr/extranet/MechanoProtein/index.php )

![Main Page](Snapshots/mainPageMPDB.png)

To display all available proteins, click on `Enter the database` button.

The current page that gathers both experimental and simulations data looks like the follwoing:

![ProteinDB](Snapshots/MPDB_all_proteins.png)

If you want to search for a specefic protein, you can use the search bar.

We show below two examples: 

In the first one the user types the PDB ID within the search entry:

![ProteinDBSearchByPDB](Snapshots/scaffoldinC2A_data.png)


In the second example, the user types the name of the protein he wants to search for, here: scaffoldin:
![ProteinDBSearchByName](Snapshots/ScaffoldingC2AByPDB.png)

In both examples, data from both experiments and simulations is displayed.


In case you want, for example, to display **only** simulation data, you can add SMD next to the protein name within the search entry (Top right). The same applies if you want to display only AFM experimental data.

Below the results of scaffoldin provided only from AFM experimental data:

![ProteinDBSearchByNameAFM](Snapshots/AFM_scaffoldinData.png)


## Annotation Interpretation and Statistics

Currently, MPDB contains 80 manually annotated proteins. The manual annotation includes a description of the unfolding process by specifying which sheet/helix unfold.

#### 1) Unfolding Annotation
If you look at the above example of Scaffoldin C2A, the annotation of the unfolding process is the following:
**[ A-I ][ A'-I ']**
This suggests that A and I beta strands break followed by A' and I' beta strands.

#### 2) Pathway

The unfolding pathway describes the steps according to which the protein unfolds.

In this example, scaffoldin unfolds through one step:
**N_U[48.6]**

The protein goes from Native to Unfolded and the Contour Length that is reported here corresponds to 48.6 nm.

Other proteins need more than one step to unfold. Consequently, intermediaite steps can be observed, we report these as I. For example, the unfolding pathway of GFP protein (PDB ID: 1EMB) is reported in MPDB as the following:

**N_I[39.3]_U[72.08]**

This annotation shows that GFP goes through an intermediaite step (I) at which the contour length corresponds to 39.3 nm.

![GFP](Snapshots/GFP.png)



**NB:** Since one AFM based force spectroscopy experiment can result in different unfolding forces at different velocities/loading rates, we display all available data points provided within the paper.
Click on the `See More` button to display the data:

![Spectrin](Snapshots/spectrin_forces_velocities.png)


#### 3) Structure and Sequence Visualization

Users can display the 3D structures of the proteins they want and these will be displayed with their corresponding sequences.
To do so click on the protein Snapshot in the **3D structure ** column.

![3D structure](Snapshots/3DstructureScaffoldin.png)

Structures are displayed with MolViewer plugin that is already used in the PDB databank. One of the many advantages of MolViewern is that it enables users to hover over protein sequence and highlight (zoom in) amino acids of their interest within the structure.

***
## Statistics

In MPDB, we provide statics of the stored data. For example, we display all forces versus velocities of the proteins according to their classification. Users have the possibility to modify the plots. (hover on the plot and click on the pen located at the top left )

![Stat1](Snapshots/ForcevsVel.png)

- The minimum and maximum forced are also displayed according to each protein class (plot below on the right)
- The mean forces of each protein fold are displayed (plot below middle)
- The total percentage of each protein type is also displayed. (plot below left)

![Stat2](Snapshots/Stat2.png)


## Contributions

Users have the possbility to contribute and make the database grow by inserting their own data to the database. To facilitate this process we provide an insertion page. If the protein structure has been solved experimentally, users must tick the `I have the PDB ID` button. A python script will scrap data (mutations, organism, SCOP annotation..etc) from the PDB databank. If the protein does not have an available structure in the PDB data bank; users will have as much information as possbile. A file containing data points can also be uploaded within this page.

![INSERT](Snapshots/InsertProteins.png)



## License
Please, refer to the licence provided in the `licence.txt` file.
