<!DOCTYPE html>
<html lang="en">
<head>

     <title> MechanoPro-DB</title>

     <meta charset="UTF-8">
   
    
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     
     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">

     <!-- SCRIPTS -->

     <!--
     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>
     -->


	<script type='text/javascript'  language='javascript'  src="https://code.jquery.com/jquery-3.5.1.js"></script>


	<script type='text/javascript' language='javascript'  src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"> </script>


	<script  src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
	<script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"> </script>
	<script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
	<script  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
	<script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"> </script>
	<script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.colVis.min.js"> </script>



     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/data_style.css">


     <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">





</head>
<body id="top">


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" >
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">MechanoPro-DB</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="index.php">Home</a></li>
                         <li class="active"><a href="all_prots.php">Data</a></li>
                         <li><a href="about-us.html">About Us</a></li>
                         <li><a href="team.html">Authors</a></li>
                         <li><a href="InsertInfo.php">Contribute</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <section>
          <div class="container">
               <div class="text-center">
                    <h1>MechanoProtein DataBase (MechanoPro-DB)</h1>

                    <br>

                    <p class="lead"> Proteins from Force Spectroscopy Experiments and Molecular Dynamics Simulations</p>
               </div>
          </div>
     </section>

     <section class="section-background"   >
          <!-- DB Content here -->
     
<table class="table" id="myTable">
        <thead>
            <tr align="center">
                <th >Name (Click for details)</th>             <!--0 -->

                <th>PDB ID</th><!--1 -->
            
                <th>SCOP annotation</th>
                <th > Highest Unfolding Forces [pN]</th>
                <th >Standard Deviation of force [pN]</th>

            
            
                <th >Velocity [nm/s]</th>
                <th >Loading Rate [pN/s]</th>
            
                
            	<th > Xu [nm]</th>
                <th > ΔG [kBT]</th>

                <th > Contour Length [nm]</th>
                <th > Koff [s-¹]</th>

                <th > Temperature (°C )</th>
                <th > Experimental Conditions</th>


                <th > Total Length (AA)</th>
                <th > Domain Coords</th>
                <th> 3D Structure (click on the protein)</th>
                
              <!--  <th> Topology</th> -->




                <th >Organism</th>
                <th >Mutations</th>
                <th >Classification</th>
                <th >Technique</th>

                <th >DOI Structure</th>

                <th >Released</th>
                <th>Authors</th>
                <th > Pulling Reference</th>
                <th >Unfolding Annotation</th>


                <th >Unfolding Pathway</th>
                <th > Mechanical Clamp </th>







            </tr>    
               </thead>
	<tbody id="myTableBody">
    <?php 
    	//session_start();
    	include 'DB_cnx.php';
		//$page = $_GET["page"];
		//$start = 5 + 5 * ($page - 1);
		//$rows = 5;
           
        // $sql ="SELECT   pdb_id, name, classification, velocity, organism, doi, loading_rate, unfolding_force, mutations, structure, released, deposition_authors, doi_pulling FROM ExperimentalProteins ORDER BY pdb_id LIMIT $start, $rows" ;
         
        $sql ="SELECT  id, pdb_id, name, clamp_motif, classification, velocity, organism, doi, loading_rate, unfolding_force, mutations, structure, released, deposition_authors, doi_pulling, unfolding_annotation,  sd_force, protein_length, domain_coords, technique, Xu, contour_length, koff,  pathway, DeltaG, other_forces, other_vel, other_lr, other_sd_force, T, experimental_conditions, author_pulling, year_pulling FROM MPDB_Proteins ORDER BY Xu" ; 
          
    	//$q = $db->query('SELECT pdb_id, COUNT(pdb_id) as counts from ExperimentalProteins GROUP BY pdb_id ORDER BY pdb_id');

    
    
  
    //    print_r( $counts);

    // print_r($r['counts']);
       /* 
        $query= "SELECT pdb_id, COUNT(pdb_id) as counts from ExperimentalProteins GROUP BY pdb_id ORDER BY pdb_id";
    	$stmt = $db->prepare('SELECT pdb_id, count(pdb_id) as counts  FROM ExperimentalProteins GROUP by pdb_id";');
		$stmt->execute(['counts' => $email, 'pdb_id' => $status]);
		$counts = $stmt->fetch();
        print $counts; 
    	*/

         /*
    	 print "<tr>";
				$result_counts = $q->fetchColumn(1);
         		$result_pdb = $q->fetchColumn();
					print $result_pdb ;
					print $result_counts;  
                	print "<td rowspan=". (int)$result_counts." ><a href=https://www.rcsb.org/structure/".$result_pdb ." target='_blank'> ".$result_pdb ."  </a> </td>";
*/
    
         foreach  ($db->query($sql) as $row) { 
                echo "<tr>";
         
         
         
               // print "<td style='font-size:90%'>".$row['name'] . "</td>";
                print "<td > <a href = details.php?pdb_id=" .$row['pdb_id']."&id=". $row['id'] . "&name=".urlencode($row['name'])."&force=".$row['unfolding_force']."&velocity=".$row['velocity']."&year=".$row['year_pulling']."&author=".urlencode($row['author_pulling']) . "&doi=".$row['doi_pulling'] ." target='_blank'> ".  $row['name'] . " </td>";


                $pdb_id =$row['pdb_id'] ;
         		//$_SESSION['pdb_id'] = $pdb_id;

				/*
				$result_counts = $q->fetchColumn(1);
         		$result_pdb = $q->fetchColumn();
				print $result_pdb ;
                if ($result_pdb == $pdb_id) {
					print $result_counts;  
                 */
                	print "<td  ><a href=https://www.rcsb.org/structure/".$pdb_id ." target='_blank'> ".$pdb_id ."  </a> </td>";
               // $count++;
              //  }


                print "<td> <a href=https://scop.berkeley.edu/pdb/code=".$pdb_id.    " target='_blank'> "    .$row['structure'] . "</td>";
         
  
                print "<td >". intval($row['unfolding_force']). "<br>" ;

         
                $other_forces= $row['other_forces'];

         		if (strlen($other_forces) != 0){
         		print "<details>";
  				print "<summary> See more</summary>";
				
         		$all_forces= explode( ', ', $other_forces );
         		$num_force=0;
         		foreach($all_forces as $force) 
               
                {
                	
                	print "<table>";
                	print "<td>";
                	$num_force++;
                	print intval($force);
                	print "<br>";
                    
					print "</td>";
                	print "</table>";
                	
                
                }

                
         print "</details>";
                }
                
         
         
          print "</td>";
         
         
                print "<td>".$row['sd_force'] .  "<br>" ;   
         
                $other_sd= $row['other_sd_force'];

         		if (strlen($other_sd) != 0){
         		print "<details>";
  				print "<summary> See more</summary>";
				
         		$all_sd= explode( ', ', $other_sd );
         		$num_sd=0;
         		foreach($all_sd as $sd) 
               
                {
                	
                	print "<table>";
                	print "<td>";
                	$num_sd++;
                	print intval($sd);
                	print "<br>";
                    
					print "</td>";
                	print "</table>";
                }
                
         print "</details>";
                }
         
          print "</td>";
         		

         
         //velocity
           //     print "<td>".$row['velocity'] . "<br>" ;   
         		print "<td>".  preg_replace('/(e[+-])(\d)$/', '${1}0$2', sprintf('%1.2e',$row['velocity'])).  "<br>";
         
                $other_vel= $row['other_vel'];

         		if (strlen($other_vel) != 0){
         		print "<details>";
  				print "<summary> See more</summary>";
				
         		$all_vel= explode( ', ', $other_vel );
         		$num_vel=0;
         		foreach($all_vel as $vel) 
               
                {
                	
                	print "<table>";
                	print "<td>";
                	$num_vel++;
                	print $vel;
                	print "<br>";
                    
					print "</td>";
                	print "</table>";
                	
                }
                
         print "</details>";
                }
         
          print "</td>";
         
         // LOADING RATE
         
                print "<td>". ($row['loading_rate']) . "<br>" ;   
         
                $other_lr= $row['other_lr'];

         		if (strlen($other_lr) != 0){
         		print "<details>";
  				print "<summary> See more</summary>";
				
         		$all_lr= explode( ', ', $other_lr );
         		$num_lr=0;
         		foreach($all_lr as $lr) 
               
                {
                	
                	print "<table>";
                	print "<td>";
                	$num_lr++;
                	print $lr;
                	print "<br>";
                    
					print "</td>";
                	print "</table>";
                }
                
         print "</details>";
                }
         
          print "</td>";
         		
         
                
         		//print "<td>". ($row['Xu']) . "</td>";
                print "<td>". (isset($row['Xu']) ? $row['Xu'] : 'N/A'). "</td>";

                
         	//	print "<td>". ($row['DeltaG']) . "</td>";
                print "<td>". (isset($row['DeltaG']) ? $row['DeltaG'] : 'N/A'). "</td>";

         
                print "<td>".$row['contour_length'] . "</td>";
         
         
                //print "<td>".($row['koff'] ) . "</td>";
                print "<td>". (isset($row['koff']) ? $row['koff'] : 'N/A'). "</td>";

                
         		print "<td>". (isset($row['T']) ? $row['T'] : 'N/A'). "</td>";
                print "<td >". (isset($row['experimental_conditions']) ? $row['experimental_conditions'] : 'N/A'). "</td>";

 //style='font-size: 11px;'

                print "<td>".$row['protein_length'] . "</td>";
                print "<td>".$row['domain_coords'] . "</td>";



         
         		print "<td > <a href = structure.php?id=" .$row['pdb_id']. "  target='_blank'><img src=ProteinSnapshots/" . $row['pdb_id'] . ".jpg width=120 height=120 ></a></td>";
         		
         		//TOPOLOGY
         		//print "<td>" .$row['pdb_id']. " <img src=topologies/" . $row['pdb_id'] . ".jpg width=200 height=250 ></td>";




                print "<td>".$row['organism'] . "</td>";
                print "<td>".$row['mutations'] . "</td>";
                print "<td>".$row['classification'] . "</td>";
                
         		print "<td>".$row['technique'] . "</td>";

         
         
         		print "<td><a href=".$row['doi']."  target='_blank'>  DOI </a> </td>";
         
                print "<td>".$row['released'] . "</td>";
                print "<td>".$row['deposition_authors'] . "</td>";
         	
         		//print"<td>"; print"<script>";
         		//print "jmolSetAppletColor(\"black\"); jmolApplet(300, \"load =pdb/1tit;restrict protein; cartoons; color structure;\", \"0\");";
         		//print"</script>" ;print"</td>";
         
         		//echo "pdb/".$row['pdb_id']." ;";
         
         		//DISPLAY 3D STRUCTURE
               // print"<td>"; print"<script>";
         	//	print "jmolInitialize(\"jmol-14.31.7/jsmol\", true); jmolSetAppletColor(\"black\"); jmolApplet(250, \"load =pdb/".$row['pdb_id']."; display not water;select protein or nucleic;cartoons only; calculate hbonds; set hbonds solid; color hbonds cyan; color structure;select *; set platformSpeed 1; \");";
         	//	print"</script>" ;print"</td>";
         
         // DISPALY SNAPSHOT INSTEAD
         
         		$doi_pulling= $row['doi_pulling'];

                        print "<td><a href=".$doi_pulling."  target='_blank'>   ".$row['author_pulling']." et al. ".$row['year_pulling']." </a>   </td>";

         	//	$links= explode( ',', $doi_pulling );
         	//	$num_link=0;
         	//	print "<td>";
         	//	foreach($links as $link) 
               // {
                //	$num_link++;
                //	print "<a href=".$link."  target='_blank'> Ref. [ ".$num_link." ] </a>";
                //	print "<br>";
                
               // }
 		//		print  "</td>";
         		//annotation DOI
				print  "<td width='1px' >" . ($row['unfolding_annotation'] ) . "</td>";
         
  
         //		$annotation_doi= $row['annotation_doi'];
         	//	$links= explode( ',', $annotation_doi );
         	//	$num_link=0;
 
         	//	print "<td>";
         	//	foreach($links as $link) 
              //  {
                //	$num_link++;
                //	print "<a href=".$link."  target='_blank'> Ref. [ ".$num_link." ] </a>";
                //	print "<br>";
                
               // }
         	//	print "</td>";
                 
         	//	print "<td> <a href = details.php?pdb_id=" .$row['pdb_id']."&id=". $row['id'] . "&name=".urlencode($row['name'])."&force=".$row['unfolding_force']."&velocity=".$row['velocity']."&year=".$row['year_pulling']."  target='_blank'> ".  $row['pathway'] . "</td>";
         		print "<td>" .  $row['pathway'] . "</td>";
         //		print "<td > " .$row['clamp_motif']. "  <img src=topology/" . $row['clamp_motif'] . ".png width=100 height=120 </td>";

			//	print "<td > " .$row['clamp_motif']. " </td>";
         
         	//	if (is_null($row['clamp_motif'])) 
              //  { 
              //  "<td> <a href=http://ptgl.uni-frankfurt.de/results.php?q=".strtolower($row['pdb_id'])."A  target='_blank'>  <img src=topology/NA.png width=80 height=80></a> </td>"; 
                
              //  }
         	//	else
         	//	{
               		 print "<td> <a href=http://ptgl.uni-frankfurt.de/results.php?q=".strtolower($row['pdb_id'])."A  target='_blank'>  <img src=topology/". $row['clamp_motif']. ".png width=80 height=80></a> </td>"; 
         	//	}
         
                echo "</tr>";

			}

        ?>
               </tbody>

    </table>


<script>
		//$.noConflict();

    	$(document).ready(function () 
         {
        
        

    	    $('#myTable').dataTable({
            
            
            
            
            		responsive: true,

                    dom: 'Bfrtip',

            		"dom": '<"top"<"left-col"B><"center-col"l><"left-col"f>>rtip',
                    pagingType: 'full_numbers',
              		pageLength: 5,
        			order: [[1, 'asc']],
                    buttons: [

            			{
                			extend: 'collection',
                           text: 'Collections (show/hide & export)',
                			className: 'custom-html-collection',
                			buttons: [
                    					'<h4>Export</h4>',
                    					'csv',
                    					'excel',
                    				'<h3 class="not-top-heading">Column Visibility</h3>',
                    				'colvis']
                        }
                    				],
            			
             			columnDefs: [
    
        				{
            				"targets": [ 16 ],
            				"visible": false
        				},
                        			{
            				"targets": [ 18 ],
            				"visible": false
        				},
                        
         				{
            				"targets": [ 17 ],
            				"visible": false
        				},
    
                        {
            				"targets": [ 21 ],
            				"visible": false
        				},
                 
                        {
            				"targets": [ 13 ],
            				"visible": false
        				},
                        {
            				"targets": [ 6 ],
            				"visible": false
        				},   
                          {
            				"targets": [ 4 ],
            				"visible": false
        				}, 
                                                 
                        {
            				"targets": [ 20 ],
            				"visible": false
        				},                           
                        {
            				"targets": [ 22 ],
            				"visible": false
        				},  
                                          
                        {
            				"targets": [ 21 ],
            				"visible": false
        				},  
                         {
            				"targets": [ 8 ],
            				"visible": false
        				},
                        {
            				"targets": [ 11 ],
            				"visible": false
        				},  
                        
            
    							]
            
    } );
        
			    
			

   		 });
         
         
        


	</script>



      
     </section>

     <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Address</h2>
                              </div>
                              <address>
                                   <p> 163 boulevard de Luminy <br> 13009 Marseille</p>
                              </address>


                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 Université Aix Marseille </p>
                              </div>
                         </div>
                    </div>

                      <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              

                              <div class="footer_menu">
                                   <h2>Quick Links</h2>
                                   <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about-us.html">About Us</a></li>
                                        <li><a href="terms.html">Terms & Conditions</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Newsletter Signup</h2>
                              </div>
                              <div>
                                   <div class="form-group">
                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required>
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

    


</body>
<style>
details {
  font: 12px "Open Sans", Calibri, sans-serif;
}

details > summary {
  background-color: #ddd;
  border: none;
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
}
details[open] > summary {
  background-color: #ccf;
}


</style>
</html>